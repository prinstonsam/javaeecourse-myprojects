package Dao;

import model.Car;

import java.util.List;

/**
 * Created by isamsonov on 10/26/15.
 */
public interface CarDao {

    void create(Car user);

    void update(Car user);

    Car getById(Long userId);

    List<Car> getAll();

    void delete(Car user);
}
