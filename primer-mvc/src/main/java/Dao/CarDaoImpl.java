package Dao;

import model.Car;

import java.util.List;

/**
 * Created by isamsonov on 10/26/15.
 */
public class CarDaoImpl implements CarDao {
    public void create(Car car) {
        //create in DB
    }

    public void update(Car car) {
        //update in db
    }

    public Car getById(Long carId) {
        //get by id
        return null;
    }

    public List<Car> getAll() {
        //get all cars
        return null;
    }

    public void delete(Car car) {
        //delete car in db

    }
}
