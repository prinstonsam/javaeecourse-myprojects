package service;

import Dao.CarDao;
import model.Car;

import java.util.List;

/**
 * Created by isamsonov on 10/26/15.
 */
public class CarService {


//    @Autowired
    private CarDao carDao;

    public void create(Car car) {
        carDao.create(car);
    }

    public void delete(Car car) {
        carDao.delete(car);
    }

    public void update(Car car) {
        carDao.update(car);
    }

    public List<Car> getAll() {
        return carDao.getAll();
    }

    public Car getById(Long carId) {
        return carDao.getById(carId);
    }

}
