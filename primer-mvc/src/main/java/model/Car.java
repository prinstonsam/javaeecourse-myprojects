package model;

/**
 * Created by isamsonov on 10/26/15.
 */
public class Car {

    private String number;
    private String carBrand;
    private Long id;

    public Car(String number, String carBrand, Long id) {
        this.number = number;
        this.carBrand = carBrand;
        this.id = id;
    }

    public Car() {
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
