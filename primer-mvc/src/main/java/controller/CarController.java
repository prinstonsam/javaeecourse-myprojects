package controller;

import model.Car;
import service.CarService;

/**
 * Simple controller
 * Created by isamsonov on 10/26/15.
 */
public class CarController {

    //@Autowired
    CarService carService;

    public static void main(String[] args) {
        CarService carService = new CarService();

        carService.create(new Car("a111aa98", "Vaz2101", 1L));

        System.out.println(carService.getById(1L));

        //.....

    }
}
