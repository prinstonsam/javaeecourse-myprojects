package service;

import model.Tag;

/**
 * Created by isamsonov on 10/29/15.
 */
public interface TagService {

    public void createTag(Tag tag);
}
