package service;

import dao.TagDAO;
import model.Tag;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by isamsonov on 10/29/15.
 */
public class TagServiceImpl implements TagService{
    ApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"springExample.xml"});

    TagDAO tagDAO;

    public void setRegionDao(TagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }

    public void createTag(Tag tag) {
        tagDAO.insert(tag);
    }
}
