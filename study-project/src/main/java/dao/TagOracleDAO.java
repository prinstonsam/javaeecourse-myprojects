package dao;

import model.Tag;

import java.sql.*;
import java.util.List;

/**
 * Created by isamsonov on 10/28/15.
 */
public class TagOracleDAO implements TagDAO{

    private Connection connection = null;

    public TagOracleDAO(Connection connection) {
        this.connection = connection;
    }

    public void insert(Tag tag) {
        PreparedStatement stat = null;
        try {
            stat = connection.prepareStatement("insert into tag values (?, ?)");
            stat.setInt(1,tag.getId());
            stat.setString(1, tag.getName());
            stat.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(int idTag) {
        PreparedStatement stat = null;
        try {
            stat = connection.prepareStatement("delete from tag where id = ?");
            stat.setInt(1,idTag);
            stat.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void update(Tag tag) {
        PreparedStatement stat = null;
        try {
            stat = connection.prepareStatement("update tag set naem = ? where id = ?");
            stat.setString(1, tag.getName());
            stat.setInt(2,  tag.getId());
            stat.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Tag getTag(Integer idTag) {
        PreparedStatement stat = null;
        try {
            stat = connection.prepareStatement("select id, name from tag where id = ?");
            stat.setInt(1, idTag);
            ResultSet resultSet = stat.executeQuery();
            return new Tag(resultSet.getInt(1), resultSet.getString(2));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Tag> getAll() {
        //TODO
        return null;
    }
}
