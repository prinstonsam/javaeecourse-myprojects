package dao;

import model.Author;

import java.util.List;

/**
 * Created by isamsonov on 10/28/15.
 */
public interface AuthorDAO {

    public void insert(Author author);

    public void delete(Author author);

    public void update(Author author);

    public Author get(Integer idAuthor);

    public List<Author> getAll();

}
