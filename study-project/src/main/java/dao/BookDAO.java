package dao;

import model.Book;

import java.util.List;

/**
 * Created by isamsonov on 10/28/15.
 */
public interface BookDAO {

    public void insert(Book book);

    public void delete(Book book);

    public void update(Book book);

    public Book get(Integer idBook);

    public List<Book> getAll();
}
