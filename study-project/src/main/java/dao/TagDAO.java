package dao;

import model.Tag;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by isamsonov on 10/28/15.
 */
public interface TagDAO {

    public void insert(Tag tag);

    public void delete(int idTag) throws SQLException;

    public void update(Tag tag);

    public Tag getTag(Integer idTag);

    public List<Tag> getAll();

}
