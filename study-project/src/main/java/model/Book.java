package model;

import java.util.List;

/**
 * Created by isamsonov on 10/28/15.
 */
public class Book {

    private int id;
    private String title;
    private List<Author> authors;
    private List<Tag> tags;

    public Book(int id, String title, List<Author> authors, List<Tag> tags) {
        this.id = id;
        this.title = title;
        this.authors = authors;
        this.tags = tags;
    }

    public Book() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

}
