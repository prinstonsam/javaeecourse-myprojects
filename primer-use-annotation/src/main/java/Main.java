import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by isamsonov on 10/26/15.
 */
public class Main {
    public static void main(String[] args) {
        Class<?> myClass = WithMyAnnotaion.class;

        for (Method method : myClass.getMethods()) {
            MyAnnotations ann = method.getAnnotation(MyAnnotations.class);

            if (ann != null) {
                try {
                    method.invoke(new Object());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
